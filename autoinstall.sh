#!/bin/bash
CONFIG=$HOME/.config/nvim

# check if neovim is installed
dpkg -s neovim &> /dev/null
if [ $? -ne 0 ]; then
  echo "NeoVim is not installed!"
  exit 1
fi

URL="https://gitlab.com/Zarato/nvim-config.git"

echo "Cloning..."
git clone $URL $CONFIG

# install plugins
sh $CONFIG/sync.sh

echo "Your NeoVim is now configured!"
