" status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" automatic quote and bracket completion
Plug 'jiangmiao/auto-pairs'
" NERDTree
Plug 'preservim/nerdtree'
" fzf
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" git
Plug 'tpope/vim-fugitive'
" surround
Plug 'tpope/vim-surround'
" rainbow brackets
Plug 'luochen1990/rainbow'
" color schemes
" Plug 'rafi/awesome-vim-colorschemes'
Plug 'gruvbox-community/gruvbox'
" latex
Plug 'lervag/vimtex'
" xcode color scheme
" Plug 'arzg/vim-colors-xcode'
" coc
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" markdown
Plug 'godlygeek/tabular' | Plug 'tpope/vim-markdown'
