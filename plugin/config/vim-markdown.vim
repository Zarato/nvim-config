" vim markdown config file

let g:vim_markdown_math = 1
" syntax highlighting
let g:vim_markdown_fenced_languages = ['html', 'python', 'viml=vim', 'bash=sh']
" Turn on spell checking
autocmd FileType mardown setlocal spell

