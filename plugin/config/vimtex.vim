" vimtex config file

let g:tex_flavor = 'latex'
let g:vimtex_view_enabled = 0
let g:vimtex_quickfix_mode = 0
let g:vimtex_compiler_enabled = 0
