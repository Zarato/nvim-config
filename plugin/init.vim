call plug#begin('~/.config/nvim/plugged')

" Load the plugins
execute "source" Dot("plugin/plugins.vim")

call plug#end()

" Load config files
for file in split(glob(Dot("plugin/config/*.vim")), "\n")
  let name = fnamemodify(file, ":t:r")

  " check if plugin exists
  if exists("g:plugs[\"" . name . "\"]")
    exec "source" file
  else
    echom "No plugin found for config file " . file
  endif
endfor
