function! Dot(path)
  " create the absolute path of nvim files
  return "~/.config/nvim/" . a:path
endfunction

" load plugins and configure them
execute "source" Dot("plugin/init.vim")

" load preferences
execute "source" Dot("preferences.vim")
