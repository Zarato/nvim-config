#!/bin/bash

# config path
CONFIG=$HOME/.config/nvim

# check for vimplug
if [ ! -f $CONFIG/autoload/plug.vim ]; then
  echo "Installing vim-plug..."
  curl -fLo $CONFIG/autoload/plug.vim \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

echo "Installing plugins..."
nvim +"PlugSnapshot! $CONFIG/snapshot.vim" +PlugUpgrade +PlugClean! \
  +PlugUpdate +qa
nvim +UpdateRemotePlugins +qa
echo "Your NeoVim is ready !"
