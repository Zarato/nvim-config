" Editing behaviour
set showmode " show what mode i'm currently editing in
set nowrap " don't wrap lines
set tabstop=2 " a tab is two spaces
set softtabstop=2
set expandtab " expand tabs
set shiftwidth=2 " number of spaces to use for autoindenting
set autoindent
set smartindent
set number " show line numbers
set showmatch " show matching paranthesis
set ignorecase " ignore case when searching
set smartcase " ignore casee if search pattern is all lowercase
syntax on " syntax highlighting
set clipboard=unnamed " normal OS clipboard interaction
set hlsearch " highlight search
set mouse=a " enable mouse click
set cc=80 " set an 80 column border for good coding style
filetype plugin indent on " activates filetype detection
" set nolist " remove hilighting of indents
" Enable 24-bit true colors if the terminal supports it
if (has("termguicolors"))
    " https://github.com/vim/vim/issues/993#issuecomment-255651605
  " let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  " let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

  set termguicolors
endif

" color schemes
set background=dark
colorscheme gruvbox
