# NeoVim Configuration files

[[_TOC_]]

## Getting started

### Prerequisites
Of course you should have NeoVim installed on your computer.

If you don't know how to install NeoVim please take a look at this
[link](https://github.com/neovim/neovim/wiki/Installing-Neovim).

### Installation
###### Unix
If you want to automatically install the config files:
```sh
curl -s -L https://gitlab.com/Zarato/nvim-config/-/raw/master/autoinstall.sh?inline=False | sh
```

If you want to do it manually:
```sh
git clone https://gitlab.com/Zarato/nvim-config $HOME/.config/nvim
cd $HOME/.config/nvim
# install vim-plug and plugins
sh sync.sh
```
###### Windows
Cry :sob:

## Screenshots
![sc1](assets/sc1.png) 
